# Release notes

## 0.1.0

__Documentation__:

* Add `docs`folder
* Add `docs/release_notes.md`

Notes: Categories used in release notes should match one of the following:

* Fixes
* Enhancements
* Documentation
* Maintenance
